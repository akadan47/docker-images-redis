#!/bin/bash
set -e

mkdir -p -m 0755 /run/redis
chown -R redis:redis /run/redis

mkdir -p -m 0755 /var/lib/redis
chown -R redis:redis /var/lib/redis

# Set initial configuration
if [ ! -f /.redis_configured ]; then
    
    if [ ! -z "${REDIS_PASS}" ]; then
        echo "=> Securing redis with a $REDIS_PASS password"
        sed 's/^# requirepass foobared/requirepass '$REDIS_PASS'/' -i /etc/redis/redis.conf
        echo "=> Done!"
        echo "========================================================================"
        echo "You can now connect to this Redis server using:"
        echo ""
        echo "    redis-cli -a $REDIS_PASS -h <host> -p <port>"
        echo ""
        echo "Please remember to change the above password as soon as possible!"
        echo "========================================================================"
    fi

    unset REDIS_PASS

    for i in $(printenv | grep REDIS_); do
        echo $i | sed "s/REDIS_//" | sed "s/_/-/" | sed "s/=/ /" | sed "s/^[^ ]*/\L&\E/" >> /etc/redis/redis.conf
    done

    echo "=> Using redis.conf:"
    cat /etc/redis/redis.conf | grep -v "requirepass"

    touch /.redis_configured
fi

exec /usr/bin/redis-server /etc/redis/redis.conf